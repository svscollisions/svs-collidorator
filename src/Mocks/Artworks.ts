export const artworks = [
  {
    id: "1",
    artist: "Mandy Mozart",
    slug: "mandymozart-cloud9",
    title:"Cloud 9",
    description: "Bla Bla Bla",
    link: "https://www.somewhereelse.com",
  },
  {
    id:"2",
    artist: "Mandy Mozart feat. MW",
    slug: "mandymozart-live",
    title: "Mandy Mozart feat. MW",
    description: "Poetry and songs originated in schamanic practise and turned into chorals and anthems about power, wishes, insignificans, misconceptions and phantasies. Mandy Mozart in a pope like action figure is preaching, yet questioning their existence and certainties. Participants can echo the choir with their phones and become part of the spatial sound performance. Electronics are performed by MW.",
    link: "https://www.mandymozart.com",
    videoId: "VlVm1nmZwv8" 
  },
  {
    id: "3",
    artist: "Mika Bankomat",
    slug: "mikabankomat-piece",
    title: "Blub",
    description: "blub blub blub",
    link: "https://www.nowhereelse.com",
  },
  {
    id: "4",
    artist: "Dirk",
    slug: "dirk",
    title: "Bloing",
    description: "blub blub blub",
    link: "https://www.nowhereelse.com",
  },
];
