import styled from "@emotion/styled";
import React from "react";
import Tilt from "react-parallax-tilt";
import { Link } from "react-router-dom";

const Container = styled.div`
  font-size: 3rem;
  line-height: 3rem;
  letter-spacing: -0.2rem;
  cursor: pointer;
  div {
    border-radius: 4rem;
    overflow: hidden;
  }
  a {
    cursor: pointer;
    
    background-color: var(--festival-primary);
    color: var(--festival-secondary);
    padding: 1.5rem;
    display: block;
  }
`;

const TimetableLink = () => {
  return (
    <Container>
      <Tilt
        className="tilt-img"
        glareEnable={true}
        glareMaxOpacity={0.8}
        glareColor="black"
        glarePosition="all"
        glareBorderRadius="0"
        tiltMaxAngleX={35}
        tiltMaxAngleY={35}
        perspective={1600}
        scale={1.1}
        transitionSpeed={2000}
        gyroscope={true}
      >
        <Link to={"/timetable"}>
          TIMETABLE ➝
        </Link>
      </Tilt>
    </Container>
  );
};
export default TimetableLink;
