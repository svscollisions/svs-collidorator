export const device = {
    xsDown: `(max-width: 520px)`,
    xsUp: `(min-width: 521px)`,
    mdDown: `(max-width: 704px)`,
    mdUp: `(min-width: 705px)`,
    lgDown: `(max-width: 1440px)`,
    lgUp: `(min-width: 1441px)`,
  };
  