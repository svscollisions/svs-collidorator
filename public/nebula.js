(function (window) {
  /* global PIXI */
  /* eslint-disable newline-after-var,prefer-template */
  /**
   *  Basic example setup
   *  @class ParticleExample
   *  @constructor
   *  @param {array} configs The emitter configurations
   */


  const pathsToTextures = (images) => {
    if (images.length <= 0) return null;
    const result = [];
    images.forEach((image) => result.push(PIXI.Texture.from(image)));
    return images;
  };

  const textures = [
    "/images/chrome-particle-1.png",
    "/images/chrome-particle-2.png",
    "/images/chrome-particle-3.png",
    "/images/chrome-particle-4.png",
    "/images/chrome-particle-5.png",
    "/images/chrome-particle-6.png",
    "/images/chrome-particle-7.png",
    "/images/chrome-particle-8.png",
    "/images/chrome-particle-9.png",
    "/images/chrome-particle-10.png",
    "/images/chrome-particle-11.png",
    "/images/chrome-particle-12.png",
    "/images/chrome-particle-13.png",
    "/images/chrome-particle-14.png",
    "/images/chrome-particle-15.png",
    "/images/chrome-particle-16.png",
    "/images/chrome-particle-17.png",
    "/images/chrome-particle-18.png",
    "/images/chrome-particle-19.png",
    "/images/chrome-particle-20.png",
    "/images/chrome-particle-21.png",
    "/images/chrome-particle-22.png",
  ];

  const chromeConfig = {
    lifetime: {
      min: 2,
      max: 5,
    },
    frequency: 0.002,
    emitterLifetime: -1,
    maxParticles: 10,
    noRotation: true,
    addAtBack: false,
    rotationSpeed: {
      min: 4,
      max: 0,
    },
    pos: {
      x: 0,
      y: 0,
    },
    alpha: {
      start: 1,
      end: 0,
    },
    scale: {
      start: 0.2,
      end: 0.2,
      minimumScaleMultiplier: 0.91,
    },
    color: {
      start: "#ffffff",
      end: "#ffffff",
    },
    speed: {
      start: 1,
      end: 1,
      minimumSpeedMultiplier: 1,
    },
    acceleration: {
      x: 0,
      y: 0,
    },
    maxSpeed: 0,
    behaviors: [
      {
        type: "alpha",
        config: {
          alpha: {
            list: [
              {
                time: 0,
                value: 1,
              },
              {
                time: 1,
                value: 0,
              },
            ],
          },
        },
      },
      {
        type: "moveSpeed",
        config: {
          speed: {
            list: [
              {
                time: 1,
                value: 1,
              },
              {
                time: 1,
                value: 2,
              },
            ],
          },
        },
      },
      {
        type: "scale",
        config: {
          scale: {
            list: [
              {
                time: 0,
                value: 0.4,
              },
              {
                time: 0.1,
                value: 0.39,
              },
              {
                time: 3,
                value: 0.32,
              },
            ],
          },
          minMult: 1,
        },
      },
      {
        type: "color",
        config: {
          color: {
            list: [
              {
                time: 0,
                value: "ffffff",
              },
              {
                time: 2,
                value: "0267FF",
              },
            ],
          },
        },
      },
      {
        type: "rotation",
        config: {
          accel: 1.2,
          minSpeed: 0,
          maxSpeed: 1,
          minStart: 0,
          maxStart: 360,
        },
      },
      {
        type: "textureRandom",
        config: {
          textures: pathsToTextures(textures),
        },
      },
      {
        type: "spawnPoint",
        config: {},
      },
    ],
  };

  const shadeTextures = [
    "/images/shade-shapes-1.png",
    "/images/shade-shapes-2.png",
    "/images/shade-shapes-3.png",
  ];

  const shadeConfig = {
    alpha: {
      start: 0,
      end: 1,
    },
    scale: {
      start: 1,
      end: 2.1,
      minimumScaleMultiplier: 1,
    },
    color: {
      start: "#e4f9ff",
      end: "#3fcbff",
    },
    speed: {
      start: 1,
      end: 1,
      minimumSpeedMultiplier: 1,
    },
    acceleration: {
      x: 0.001,
      y: 0,
    },
    maxSpeed: 0,
    startRotation: {
      min: 0,
      max: 360,
    },
    noRotation: false,
    rotationSpeed: {
      min: 0.1,
      max: 0.1,
    },
    lifetime: {
      min: 10,
      max: 30,
    },
    blendMode: "normal",
    frequency: 0.01,
    emitterLifetime: -0.1,
    maxParticles: 3,
    pos: {
      x: 0,
      y: 0,
    },
    addAtBack: false,
    spawnType: "burst",
    particlesPerWave: 1,
    particleSpacing: 8,
    angleStart: 45,
    behaviors: [
      {
        type: "alpha",
        config: {
          alpha: {
            list: [
              {
                time: 0,
                value: 0,
              },
              {
                time: 1,
                value: 1,
              },
            ],
          },
        },
      },
      {
        type: "moveSpeed",
        config: {
          speed: {
            list: [
              {
                time: 1,
                value: 1,
              },
              {
                time: 1,
                value: 2,
              },
            ],
          },
        },
      },
      {
        type: "scale",
        config: {
          scale: {
            list: [
              {
                time: 0,
                value: 1,
              },
              {
                time: 1,
                value: 1.5,
              },
            ],
          },
          minMult: 1,
        },
      },
      {
        type: "rotation",
        config: {
          accel: 0.2,
          minSpeed: 0,
          maxSpeed: 0.001,
          minStart: 0,
          maxStart: 360,
        },
      },
      {
        type: "textureRandom",
        config: {
          textures: pathsToTextures(shadeTextures),
        },
      },
      {
        type: "spawnPoint",
        config: {},
      },
    ],
  };

  console.log(
    "Config Behaviors:",
    PIXI.particles.upgradeConfig(shadeConfig, ["url"]).behaviors
  );

  const configs = {
    chromeConfig,
    shadeConfig,
  };

  const getRandomNumber = (min, max) =>
    Math.floor(Math.random() * (max - min + 1) + min);

  class ParticleExample {
    constructor(configs) {
      const foregroundCanvas = document.getElementById("foregroundStage");
      const backgroundCanvas = document.getElementById("backgroundStage");
      // Basic PIXI Setup
      const rendererOptionsForeground = {
        width: window.innerWidth,
        height: window.innerHeight,
        view: foregroundCanvas,
        transparent: "notMultiplied",
      };
      const rendererOptionsBackground = {
        width: window.innerWidth,
        height: window.innerHeight,
        view: backgroundCanvas,
        transparent: "notMultiplied",
      };

      this.foregroundStage = new PIXI.Container();
      this.backgroundStage = new PIXI.Container();
      this.chromeEmitter = null;
      this.shadeEmitter = null;
      this.foregroundRenderer = new PIXI.Renderer(rendererOptionsForeground);
      this.foregroundRenderer.view.style["touch-action"] = "auto"; // Solve native scrolling issue on mobile https://github.com/pixijs/pixijs/issues/2410#issuecomment-281231330
      this.backgroundRenderer = new PIXI.Renderer(rendererOptionsBackground);
      this.backgroundRenderer.view.style["touch-action"] = "auto"; // Solve native scrolling issue on mobile https://github.com/pixijs/pixijs/issues/2410#issuecomment-281231330
      this.bg = null;
      this.updateHook = null;
      this.containerHook = null;

      // Calculate the current time
      let elapsed = Date.now();

      // Update function every frame
      const update = () => {
        // Update the next frame
        requestAnimationFrame(update);

        const now = Date.now();
        if (this.chromeEmitter) {
          // update emitter (convert to seconds)
          this.chromeEmitter.update((now - elapsed) * 0.001);
        }
        if (this.shadeEmitter) {
          // update emitter (convert to seconds)
          this.shadeEmitter.update((now - elapsed) * 0.001);
        }

        // call update hook for specialist examples
        if (this.updateHook) {
          this.updateHook(now - elapsed);
        }

        elapsed = now;

        // render the stages
        this.foregroundRenderer.render(this.foregroundStage);
        this.backgroundRenderer.render(this.backgroundStage);
      };

      // Resize the canvas to the size of the window
      window.onresize = () => {
        foregroundCanvas.width = window.innerWidth;
        foregroundCanvas.height = window.innerHeight;
        this.foregroundRenderer.resize(foregroundCanvas.width, foregroundCanvas.height);
        backgroundCanvas.width = window.innerWidth;
        backgroundCanvas.height = window.innerHeight;
        this.backgroundRenderer.resize(backgroundCanvas.width, backgroundCanvas.height);
      };
      window.onresize();
      
      const loader = PIXI.Loader.shared;

      // Preload the particle images and create PIXI textures from it
      // if not preloaded in config already on initialisation
      // console.log(configs)
      // var urls = configs.chromeConfig.behaviors
      //   .find((behavior) => behavior.type === "textureRandom")
      //   .config.textures.slice();
      // urls = urls.concat(
      //   configs.shadeConfig.behaviors
      //     .find((behavior) => behavior.type === "textureRandom")
      //     .config.textures.slice()
      // );
      // console.log(urls)
      // for (var i = 0; i < urls.length; ++i) {
      //   loader.add("img" + i, urls[i]);
      // }

      loader.load(() => {

        // Shade Emitter:
        // Create the new emitter and attach it to the stage
        const [emitterShadeContainer] = [
          new PIXI.Container(),
          "PIXI.Container",
        ];
        // Apply motion blur filter
        const blurShadeFilter = new PIXI.filters.BlurFilterPass();
        // blurFilter.blur = 0;
        blurShadeFilter.enabled = false;
        // Anti Aliasing
        const aaShadeFilter = new PIXI.filters.FXAAFilter();
        aaShadeFilter.enabled = false;
        emitterShadeContainer.filters = [blurShadeFilter, aaShadeFilter];

        this.backgroundStage.addChild(emitterShadeContainer);

        window.shadeEmitter = this.shadeEmitter = new PIXI.particles.Emitter(
          emitterShadeContainer,
          configs.shadeConfig
        );

        // Center on the stage
        this.shadeEmitter.updateOwnerPos(window.innerWidth/2, window.innerHeight/2);

        // Chrome Emitter:
        // Create the new emitter and attach it to the stage
        const [emitterContainer] = [
          new PIXI.Container(),
          "PIXI.Container",
        ];
        // Apply motion blur filter
        const blurFilter = new PIXI.filters.BlurFilterPass();
        // blurFilter.blur = 0;
        blurFilter.enabled = false;
        // Anti Aliasing
        const aaFilter = new PIXI.filters.FXAAFilter();
        aaFilter.enabled = false;
        emitterContainer.filters = [blurFilter, aaFilter];

        this.foregroundStage.addChild(emitterContainer);
        window.chromeEmitter = this.chromeEmitter = new PIXI.particles.Emitter(
          emitterContainer,
          configs.chromeConfig
        );

        // Position pon the stage
        this.chromeEmitter.updateOwnerPos(0, 0);

        // disable context menu
        document.body.addEventListener("contextmenu", (e) => {
          e.preventDefault();
          return false;
        });

        // Start the update
        update();

        // for testing and debugging
        window.destroyEmitter = () => {
          this.chromeEmitter.destroy();
          this.chromeEmitter = null;
          this.shadeEmitter.destroy();
          this.shadeEmitter = null;
          window.destroyEmitter = null;
          this.foregroundRenderer.render(this.foregroundStage);
          this.backgroundRenderer.render(this.backgroundStage);
        };
      });
    }
  }

  // Assign to global space
  // window.ParticleExample = ParticleExample;

  const STARTING_FREQUENCY = 0.0001;
  const example = new ParticleExample(configs);

  let frameTimes = [];
  example.updateHook = (elapsed) => {
    if (!example.chromeEmitter) return;

    frameTimes.push(elapsed);

    // Link playback to react app LayoutStore playing state.
    this.chromeEmitter.emit = window.isPlaying;
    window.residencyVisibility ?
      this.foregroundStage.style.visibility = "visible" :
      this.foregroundStage.style.visibility = "hidden"
    
    window.festivalVisibility ?
      this.backgroundStage.style.visibility = "visible" :
      this.backgroundStage.style.visibility = "hidden"
  
    // this.chromeEmitter.resetPositionTracking();
    this.chromeEmitter.updateSpawnPos(
      getRandomNumber(0, window.innerWidth),
      getRandomNumber(0, window.innerHeight)
    );
    this.shadeEmitter.updateSpawnPos(
      getRandomNumber(0, window.innerWidth),
      getRandomNumber(0, window.innerHeight)
    );
  };

  // reset settings on container change
  example.containerHook = () => {
    if (!example.chromeEmitter) return;
    frameTimes.length = 0;
    example.chromeEmitter.frequency = STARTING_FREQUENCY;
  };
  window.particles = example;
})(window);

console.log("2000");
